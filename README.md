# Histopathologic Cancer Detection
My attempt to contribute to [Histopathologic Cancer Detection](https://www.kaggle.com/c/histopathologic-cancer-detection/overview).

## Introduction (from Kaggle)
In this competition, you must create an algorithm to identify metastatic cancer in small image patches taken from larger digital pathology scans. The data for this competition is a slightly modified version of the [PatchCamelyon (PCam) benchmark dataset](https://github.com/basveeling/pcam) (the original PCam dataset contains duplicate images due to its probabilistic sampling, however, the version presented on Kaggle does not contain duplicates).

## Approach
I will create a dataframe containing image paths. I will apply basic augmentation and use flow_images_from_directory to feed images to the model. The model will be headless, unfrozen and pre-trained Xception model (Chollet 2017) which I will train and validate on provided data. I will load images in higher resolution to better fit the model.

## Evaluation
Submissions are evaluated on area under the [ROC curve](https://en.wikipedia.org/wiki/Receiver_operating_characteristic) between the predicted probability and the observed target.

## References and Links
1. https://www.kaggle.com/qitvision/a-complete-ml-pipeline-fast-ai
1. https://www.kaggle.com/vbookshelf/cnn-how-to-use-160-000-images-without-crashing
1. https://www.kaggle.com/fmarazzi/baseline-keras-cnn-roc-fast-10min-0-925-lb
1. https://www.kaggle.com/kokecacao/generating-cancer-jigsaw